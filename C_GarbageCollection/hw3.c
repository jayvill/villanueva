#include <stdlib.h>
#include <unistd.h>
#include <stdio.h>
#include <string.h>
#include <assert.h>
#include "hw3.h"
struct memory_region{
    size_t * start;
    size_t * end;
};
/*Each chunk of memory in the free list begins with a header describing the block*/
typedef struct header{
	unsigned int size;
	struct block *next;
}header_t;

typedef size_t *ptr;

struct memory_region global_mem;
struct memory_region heap_mem;
struct memory_region stack_mem;
struct chunk chunk_mem;

//grabbing the address and size of the global memory region from proc 
void init_global_range(){
    char file[100];
    char * line=NULL;
    size_t n=0;
    size_t read_bytes=0;
    size_t start, end;

    sprintf(file, "/proc/%d/maps", getpid());
    FILE * mapfile  = fopen(file, "r");
    if (mapfile==NULL){
        perror("opening maps file failed\n");
        exit(-1);
    }

    int counter=0;
    while ((read_bytes = getline(&line, &n, mapfile)) != -1) {
        if (strstr(line, "hw3")!=NULL){
            ++counter;
            if (counter==3){
                sscanf(line, "%lx-%lx", &start, &end);
                global_mem.start=(size_t*)start;
                global_mem.end=(size_t*)end;
            }
        }
        else if (read_bytes > 0 && counter==3){
            //if the globals are larger than a page, it seems to create a separate mapping
            sscanf(line, "%lx-%lx", &start, &end);
            if (start==global_mem.end){
                global_mem.end=(size_t*)end;
            }
            break;
        }
    }
    fclose(mapfile);
}
/*
   void init_gc() {
   fprintf(stderr,"Initalizing...\n");
   heap_mem.start=malloc(512);//the first call to malloc returns a pointer to the beginning of the
//allocated memory (beginning of the heap)
stack_mem.start = &heap_mem.start;
init_global_range();
free(heap_mem.start);
}*/
/*
   Mark phase marks all reachable and allocated descendants of the root nodes

   To identify root objects, you'll just have to traverse the entire stack and global memory area and look for anything that is a valid pointer. A pointer is valid if it's value is somewhere within the range of the heap. 

   Once you identify a valid pointer, you'll have to go to the chunk that it corresponds to, mark that chunk and then check if there are any other valid pointers within that chunk. 
 */

/*
   if p points to some word in an allocated block, returns a pointer b to the beginning of that block
 */
/*ptr isPtr(ptr p){
  int inuse = 0;
  if()//check to see if p points to some allocated block
  return NULL;

  }*/

/*    
      if p points to some word in an allocated block, returns a pointer b to the beginning of that block 
 */
ptr isPtr(ptr p){//used for heap
    //void *search_start = global_mem.start;
    //void *current_chunk = heap_mem.start;
    //while(search_start < stack_mem.end){//stack grows towards lower adresses
    //printf("inside isPtr function\n");
    size_t *current_chunk = p; 
    size_t size  = *(size_t*) (current_chunk - sizeof(size_t)) & ~1;//get the size of the current chunk
    int inuse = 0;//set in use to zero
    void *next_chunk = current_chunk + size;//get the next chunk
    if(next_chunk < heap_mem.end){//if the chunk is in range of the heap
        inuse = *(size_t*)(next_chunk - sizeof(size_t)) & 1;//use the LSB to see if prev chunk was allocated
    }
    if(inuse){
        return *current_chunk;//return a pointer to the allocated chunk to be marked
    }
    else return NULL;
    //}
    //printf("finished isPtr\n");
}
//don't forget, using typedef void *ptr;
/*------------------------------------------------------
  markBlock():
  mark it by toggling the third bit of the heap chunk
  --------------------------------------------------------*/
void markBlock(ptr b){
    *b |= 1 << 3;//set bit 3 to 1
    //printf("called markBlock()\n");
}
/*------------------------------------------------------
  length():
  (SUPPOSEDLY)
  returns the length in words(excluding header) of block b
  --------------------------------------------------------*/
size_t length(ptr b){
    size_t length = *(size_t*)(b - sizeof(size_t)) & ~1;
    return length;
    //printf("got the length\n");
    

}
/*------------------------------------------------------
  blockMarked():
  Returns 1 if block already marked or 0 if unmarked
  --------------------------------------------------------*/
int blockMarked(ptr b){
    //check the third LSB
    int bit = *b & (1 << 3);//That will put the value of bit x into the variable bit.
    if(bit == 1){//if marked
        return 0;//is not marked
    }
    else return 1; //is marked
}
/*------------------------------------------------------
  unmarkBlock():
  changes the status of block b from marked to unmarked
  --------------------------------------------------------*/
void unmarkBlock(ptr b){
    if(blockMarked(b)){
        *b |= 0 << 3; //set bit 3 to 0 (unmarked)
        //printf("Block unmarked\n");
        return;
    }
    else{ 
        //printf("Block already unmarked\n");
        return;
    }
}
/*------------------------------------------------------
  blockAllocated():
  returns 1 if block b is allocated
  --------------------------------------------------------*/
int blockAllocated(ptr b){
    size_t *current_chunk = b;
    size_t size  = *(size_t*) (current_chunk - sizeof(size_t)) & ~1;//get the size of the current chunk
    size_t *next_chunk = current_chunk + size;//get the next chunk
    if(next_chunk < heap_mem.end){//if the chunk is in range of the heap
        int bit = *next_chunk & (1 << 1);// put the value of bit 1 into variable bit
        if(bit == 1)//if allocated
        {
            return 1;
        }
        else return 0;
    }
    else{ 
        printf("in blockAllocated: nextChunk not in range of heap\n");
        return -1;
    }
}
/*------------------------------------------------------
  nextBlock():
  returns the successor of block b in the heap  
  --------------------------------------------------------*/
ptr nextBlock(ptr b){
    size_t *current_chunk = b;
    size_t size  = *(size_t*) (current_chunk - sizeof(size_t)) & ~1;//get the size of the current chunk
    size_t *next_chunk = current_chunk + size;//get the next chunk
    if(next_chunk < heap_mem.end){//if the chunk is in range of the heap
        return *next_chunk;//return a pointer to the sucessor
    }
    else{//when next_chunk is not in the range of the heap
        printf("in nextBlock: next_chunk not in range of heap\n");   
        return NULL;
    } 
}
/*-------------------
  MARK
  ---------------------*/ 
void mark(ptr p){
    //printf("marking");
    ptr b = isPtr(p);
    int len, i;
    if(b==NULL){
        return;
    }
    if(blockMarked(b))
        return;
    markBlock(b);
    len = length(b);
    printf("the length is %d", len);
    for(i = 0; i< len; ++i)
        mark(b[i]);
    return;
}
/*-------------------
  SWEEP    
  ---------------------*/
void sweep(ptr b, ptr end){
    printf("sweeping...\n");
    while(b < end){
        if(blockMarked(b))
            unmarkBlock(b);
        else if (blockAllocated(b))
            free(b);
        b = nextBlock(b);
    }
    return;
}
/*------------------------------------------------------
  checkGlobals():
  identity root objects in the global region
  if found mark it and its descendants
  --------------------------------------------------------*/
void checkGlobals(){
    //printf("checking globals\n");
    size_t *current = global_mem.start;
    /*CHECK EACH GLOABL FOR A POINTER INTO THE HEAP*/
    while(current < global_mem.end){//while were not at the end of the gloabl region
        printf("inside while loop of checkGlobals\n");
        //size_t *heap_walker = heap_mem.start;
        //while(heap_walker < heap_mem.end){
            //if(current == heap_walker){//if we find a pointer into the heap
                mark(current);//mark it and follow it
            //}
            //heap_walker++;
        //}
        current++;
    }
    //return NULL;
}
/*------------------------------------------------------
  checkStack():
  identity root objects in the stack region 
  if found mark it and its descendants
  --------------------------------------------------------*/
void checkStack(){
    printf("checking stack\n");
    size_t *current = stack_mem.start;
    /*CHECK EACH STACK POINTER INTO THE HEAP*/
    while(current > stack_mem.end){
        //size_t *heap_walker = heap_mem.start;
        //while(heap_walker < heap_mem.end){
            //if(current == heap_walker){//if we find a pointer into the heap
                mark(current);//mark it and follow it
            //}
           // heap_walker++;
        //}
        current++;    
    }
    //return NULL;
}

void init_gc() {
    int x;
    //stack_mem.end = &x;
    heap_mem.end=sbrk(0);
    fprintf(stderr,"Initalizing...\n");
    heap_mem.start=malloc(512);//the first call to malloc returns a pointer to the beginning of the
    //allocated memory (beginning of the heap)
    stack_mem.start = &heap_mem.start;
    init_global_range();
    free(heap_mem.start);
}
void gc(){
    char stack_var;
    stack_mem.end = &stack_var;
    //checkGlobals();
    //checkStack();
    size_t *stackPtr = stack_mem.start;
    mark(stackPtr);
    mark(global_mem.start);
    sweep(heap_mem.start, heap_mem.end);
}



//void mark(int *ptr){
//if(){//heap grows from low to high adresses
//  return;
//}
//int bit = *ptr & (1 << 3);
//if()
//{

// }
//else{
//mark it by toggling the third bit of the heap chunk
// *current |= 1 << 3;//set bit 3 to 1
//   mark(ptr++);
// }

/*
//identity root objects
char etext, edata, end;
int *data_start = &edata;//pointer to beginning of init data segment
int *data_end = &end;//pointer to start of uninitalized data segment
int *heap_begin = heap_mem.start;
while(data_start != data_end){
//check if there are any valid pointers into heap
while(heap_begin != heap_mem.end)
{
if(*data_start  == *heap_begin)// if its value is somewhere within the range of the heap
{
//then its a valid pointer so go to the chunk that it corresponds to and mark it

}      
}        
data_start++;//increment the pointer
}
while(stack_mem.start < stack_mem.end){

}*/            
//}

/*
   void gc() {
//char stack_var;
int x;
stack_mem.end = &x;    
heap_mem.end=sbrk(0);

long int heap_size = heap_mem.end - heap_mem.start;
long int stack_size = (stack_mem.end - stack_mem.start);
long int allocated_chunks = 0;
	long int freed_chunks = 0;
	long int allocated_bytes = 0;
	long int freed_bytes = 0;
	
	void *current_chunk = heap_mem.start;

    //find the roots:
        //NOTE:Root nodes correspond to locations not in the heap that contain pointers into the heap
    
    //---------------------------------------------------------------
	while(current_chunk < heap_mem.end){//walk through the heap
		size_t size = *(size_t*)((current_chunk) - sizeof(size_t)) & ~1;//mask the lsb b/c its not part of the size	
		void *next_chunk = current_chunk + size; 
		int inuse = 0;
		if(next_chunk < heap_mem.end)
			inuse = *(size_t*)(next_chunk - sizeof(size_t)) & 1;//mask everything besides the lsb to get the info about prev chunk
		if(inuse) {//if the chunk is in use
			allocated_chunks++;
			allocated_bytes += size;
		}//printf("chunk %p is inuse\n",current_chunk);
		else {//if the chunk is not in use
			freed_chunks++;
			freed_bytes += size;
		}//printf("chunk %p not in inuse\n", current_chunk);
		//printf("chunk size = %zu\n", size);
		current_chunk += size;
	}
//	printf("[heapsize: %ld,allocatedChunks: %ld, freedChunks: %ld, allocatedBytes: %ld, freedBytes: %ld, stackSize: %ld]\n",heap_size, allocated_chunks, freed_chunks, allocated_bytes, freed_bytes, stack_size);
}
*/










