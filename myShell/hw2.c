#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <string.h>
#include <errno.h>
#include <sys/types.h>
#include <signal.h>
#include <fcntl.h>

#define MAX_SIZE  512
#define ARG_MAX 128
#define SEPERATORS "\t \n"
void parse(int *nargs, char* cmd, char* args[]){
	int i = 1;
	// arg[0] is always the command
	args[0] = strtok(cmd, "\t \n");
	while(i<ARG_MAX && (args[i] = strtok(NULL,"\t \n")) != NULL)
		i++;
/*	while(*cmd != '\0'){
		while(*cmd == ' ' || *cmd == '\t'||*cmd == '\n')
			*cmd++ = '\0';
		*args++ = cmd;
		while(*cmd != '\0' && *cmd != ' ' && *cmd != '\t' && *cmd != '\n')
			cmd++;
	}
		*args = '\0';
*/
	*nargs = i;
	int j;
	for(j = 0; j<i; ++j)
		printf("args[%d] = %s\n",j,args[j]);
}
void execute(char **args, int *nargs){
	pid_t pid;
	int status;
	if((pid = fork()) < 0){
		printf("error: forking failed");
		exit(1);
	}
	else if(pid == 0){
		if(*nargs > 1){
                        if(!strcmp(">", args[1])){
                                printf("Using >\n");
                                int fd = open(args[2], O_WRONLY | O_CREAT);
                                if(fd < 0) printf("ERROR\n");
                                dup2(fd,1); 
                                close(fd);
                                printf("This > message is to console. But goes to a file\n");
                        }
			if(!strcmp(">>", args[1])){
                                printf("Using >>\n");
                                int fd = open(args[2], O_WRONLY | O_APPEND);
                                if(fd < 0) printf("ERROR\n");
                                dup2(fd,1);
                                close(fd);
                                printf("This >> message is to console. But goes to a file\n");
                        }
			if(!strcmp("<", args[1])){
                                printf("Using <\n");
                                int fd = open(args[2], O_WRONLY | O_APPEND);
                                if(fd < 0) printf("ERROR\n");
                                dup2(fd,1);
                                close(fd);
                                printf("This < message is to console. But goes to a file\n");
                        }
                }
		if(execvp(*args,args) < 0){
			printf("Exec Failed\n");
			exit(1);
		}
	}
	else{
		printf("Waiting for Child (%d)\n\n", pid);
		
		while(wait(&status)!=pid){;}
		
		if(WIFEXITED(status)){
                        printf("\nChild %d terminated normally with exit status = %d\n",pid,WEXITSTATUS(status));
                }
                
		else printf("\nChild %d terminated abnormally\n", pid);
                printf("Child process (%d) complete\n", pid);
	}
}
static void intHandler(int dummy){
	pid_t pid;
	printf(": CAUGHT signal %d\n", dummy);
	
	while((pid = wait(-1,NULL,0))>0)
		printf("Handler reaped child %d\n", (int)pid);
	
	if(errno != ECHILD)
		printf("wait pid error");
	return;
}
static void sigHandler(int signal){
	
	pid_t pid;
	printf(": CAUGHT signal %d\n", signal);
	
	while((pid = wait(-1,NULL,0))>0)
                printf("Handler reaped child %d\n", (int)pid);
        
	if(errno != ECHILD)
                printf("wait pid error");
        return;

}
int main(int argc, char **argv){// char **envp){
	
	char cmd[512];
	char *args[MAX_SIZE];
	//char delim[] = "\n";
	//pid_t pid;
	//int status;
	
	signal(SIGINT,intHandler);//install the signal handler for ctrl+C
	signal(SIGTSTP,sigHandler);//install the signal handler for ctrl+Z
	
	while(1){
		//int i;
		int num_args = 0;
		//int len = strlen(cmd);
		
		printf("$ ");
		fgets(cmd, 512, stdin);
		//cmd[len-1] = '\0';
		parse(&num_args, cmd, args);
		
		if(!strcmp(args[0],"exit")){
			exit(0);
		}

		execute(args, &num_args);
	}

	return 0;
	}
