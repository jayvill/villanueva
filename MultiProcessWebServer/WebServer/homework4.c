#include <fnmatch.h>
#include <errno.h>
#include <netinet/in.h>
#include <pthread.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <sys/select.h>
#include <sys/socket.h>
#include <sys/stat.h>
#include <sys/types.h>
#include <sys/uio.h>
#include <unistd.h>
#include <dirent.h>
#include <netdb.h>
#include <arpa/inet.h>
#include <netinet/in.h>
#include <assert.h>

#define BACKLOG (10)
#define AMAZON_IP "54.157.201.48" 
#define GET_INFO "GET / HTTP/1.1\r\nHOST:ec2-54-157-201-48.compute-1.amazonaws.com:8080\r\n\r\n"
/* char* parseRequest(char* request)
 * Args: HTTP request of the form "GET /path/to/resource HTTP/1.X" 
 *
 * Return: the resource requested "/path/to/resource"
 *         0 if the request is not a valid HTTP request 
 * 
 * Does not modify the given request string. 
 * The returned resource should be free'd by the caller function. 
 */
char* parseRequest(char* request) {
    //assume file paths are no more than 256 bytes + 1 for null. 
    char *buffer = malloc(sizeof(char)*257);
    memset(buffer, 0, 257);

    if(fnmatch("GET * HTTP/1.*",  request, 0)) return 0; 

    sscanf(request, "GET %s HTTP/1.", buffer);
    return buffer; 
}//use parse request to parse the client command


char* readFileByBytes(char* name){
    FILE *f1 = fopen(name,"r");
    fseek(f1, 0, SEEK_END);
    long len = ftell(f1);
    char *ret = malloc(len);
    fseek(f1, 0, SEEK_SET);
    fread(ret, 1, len, f1);
    fclose(f1);
    return ret;
}

void not_found(int client){
    char buf[256];

    sprintf(buf, "HTTP/1.0 404 NOT FOUND\r\n");
    send(client, buf, strlen(buf), 0);
    send(client, buf, strlen(buf), 0);
    sprintf(buf, "Content-Type: text/html\r\n");
    send(client, buf, strlen(buf), 0);
    sprintf(buf, "\r\n");
    send(client, buf, strlen(buf), 0);
    sprintf(buf, "<!DOCTYPE html>\r\n");
    sprintf(buf, "<HTML><TITLE>Not Found</TITLE>\r\n");
    send(client, buf, strlen(buf), 0);
    sprintf(buf,"<h2>HTTP 404 ERROR</h2>\r\n");
    send(client, buf, strlen(buf), 0);
    sprintf(buf, "<BODY><P>The server could not fulfill\r\n");
    send(client, buf, strlen(buf), 0);
    sprintf(buf, "your request because the resource specified\r\n");
    send(client, buf, strlen(buf), 0);
    sprintf(buf, "is unavailable or nonexistent.\r\n");
    send(client, buf, strlen(buf), 0);
    sprintf(buf, "</BODY></HTML>\r\n");
    send(client, buf, strlen(buf), 0);

}

void handleRequest(int fd){//the new socket fd is ready to send() and recv()
    struct stat file_stat;
    struct dirent *dirent_ptr;
    DIR *dip;

    char buf[4096];
    //zero out the buffer to be safe
    memset(buf, 0, sizeof(buf));
    int numbytes;
    char currentPath[FILENAME_MAX];
    int depth;
    //char content[4096];
    char full_path[512];
    //read from file descriptor into buffer until we hit \r\n\r\n
    //recv returns the number of bytes actually read into the buffer
    //
    //int num_bytes = recv(fd, buf, 4096, 0);
    int num_bytes = 0;
    char *request;
    while(1){
        //accumulate into buffer
        num_bytes += recv(fd, &buf[num_bytes], 4096-num_bytes,0);
        char *condition = strstr(buf, "\r\n\r\n");
        if(condition != NULL){
            // parse the request
            request = parseRequest(buf);
            break;
        }
    }
    //put a period(.) before the request so it becomes ./request
    int read_fd;
    char final_file[4096];
    //zero out the buffer cuz its on the stack
    memset(final_file, 0, sizeof(final_file));
    final_file[0] = '.';
    strncat(final_file, request,4094);
    read_fd = open(final_file,0);
    //int read_fd;
    //printf("%s", final_file);
    //check if valid file
    //char *path = realpath(request,full_path);
    printf("%s\n", final_file);
    if(stat(final_file, &file_stat)==0){
        //the file exists so send OK to Client
        if(send(fd, "HTTP/1.0 200 OK\r\n", 17, 0) == -1){
            perror("send");
        }
        //check if the file is a directory
        //change the working directory to the request if it is indeed a directory
        //use struct stat
        //if(stat(request, &file_stat) == 0){
        if(S_ISDIR(file_stat.st_mode)){ //check if it is a directory
            if((dip = opendir(request))==NULL){
                perror("dir");
            }
            //store current working directory
            if((getcwd(currentPath, FILENAME_MAX)) == NULL)
                perror("getcwd");
            //read all items in directory
            while((dirent_ptr = readdir(dip))!=NULL){
                if(strcmp(dirent_ptr->d_name,request)==0)
                    printf("%s", request);
            }
        }
        else{//assume its a file 
            //send out the content type to the Client
            if(strstr(final_file, ".html") != NULL){//get html content header
                send(fd, "Content-Type: text/html; charset=UTF-8\r\n\r\n",42,0);            
                char *file_read = readFileByBytes(final_file);
                write(fd, file_read, 4096) ;           
            }
            if(strstr(final_file, ".txt") != NULL){//get txt content header
                send(fd, "Content-Type: text/plain\r\n\r\n",28,0);
                char *file_read = readFileByBytes(final_file);
                write(fd, file_read, 4096) ;
            }
            if(strstr(final_file, ".jpeg") != NULL){//get jpeg content header
                send(fd, "Content-Type: image/jpeg\r\n\r\n",28,0);
                char *file_read = readFileByBytes(final_file);
                write(fd, file_read, 4096) ; 
            }
            if(strstr(final_file, ".gif") != NULL){//get gif content header
                send(fd, "Content-Type: image/gif\r\n\r\n",27,0);
                char *file_read = readFileByBytes(final_file);
                write(fd, file_read, 4096) ;
            }
            if(strstr(final_file, ".png") != NULL){//get png content header
                send(fd, "Content-Type: image/png\r\n\r\n",27,0);
                char *file_read = readFileByBytes(final_file);
                write(fd, file_read, 4096) ;
            }
            if(strstr(final_file, ".pdf") != NULL){//get pdf content header
                send(fd, "Content-Type: application/pdf\r\n\r\n",33,0);
                char *file_read = readFileByBytes(final_file);
                write(fd, file_read, 4096) ; 
            }
        }
    }
        else not_found(fd);
    return;
    }//end handleRequest

    /*
       if
       strstr
       read

       nb = recv(fd,buf,n,0)
       nb += (fd. &buf[nb],n-nb, 0)
     */

    /*void *echo(void *param){
      int connection = (int) param;
      while(1){
      char buf[100];
      memset(buf, 0, sizeof(buf));
    //now recieve message from the client...
    int len = recv(connection, buf, sizeof(buf), 0);
    printf("len = %d\n", len);
    if(len>0){//if there is 
    char *msg = "echoing back: " ;
    send(connection, msg, strlen(msg), 0);
    send(connection, buf, len, 0);
    printf("echoing back: %s to connection #%d\n", buf, connection);
    }
    else {
    printf("client closed connection %d\n", connection);
    close(connection);
    printf("connection closed\n");
    break;
    }
    }
    }*/

    /* Your program should take two arguments:
     * 1) The port number on which to bind and listen for connections, and
     * 2) The directory out of which to serve files.
     */
    int main(int argc, char** argv) {
        char content[256];

        /* For checking return values. */
        int retval;
        /* Read the port number from the first command line argument. */
        int port = atoi(argv[1]);

        //SOCKET 
        /* Create a socket to which clients will connect. */
        int server_sock = socket(AF_INET6, SOCK_STREAM, 0);
        if(server_sock < 0) {
            perror("Creating socket failed");
            exit(1);
        }

        /* A server socket is bound to a port, which it will listen on for incoming
         * connections.  By default, when a bound socket is closed, the OS waits a
         * couple of minutes before allowing the port to be re-used.  This is
         * inconvenient when you're developing an application, since it means that
         * you have to wait a minute or two after you run to try things again, so
         * we can disable the wait time by setting a socket option called
         * SO_REUSEADDR, which tells the OS that we want to be able to immediately
         * re-bind to that same port. See the socket(7) man page ("man 7 socket")
         * and setsockopt(2) pages for more details about socket options. */
        int reuse_true = 1;
        retval = setsockopt(server_sock, SOL_SOCKET, SO_REUSEADDR, &reuse_true,
                sizeof(reuse_true));
        if (retval < 0) {
            perror("Setting socket option failed");
            exit(1);
        }

        /* Create an address structure.  This is very similar to what we saw on the
         * client side, only this time, we're not telling the OS where to connect,
         * we're telling it to bind to a particular address and port to receive
         * incoming connections.  Like the client side, we must use htons() to put
         * the port number in network byte order.  When specifying the IP address,
         * we use a special constant, INADDR_ANY, which tells the OS to bind to all
         * of the system's addresses.  If your machine has multiple network
         * interfaces, and you only wanted to accept connections from one of them,
         * you could supply the address of the interface you wanted to use here. */


        struct sockaddr_in6 addr;   // internet socket address data structure
        addr.sin6_family = AF_INET6;

        //htons-host to network short.(assume host byte order isnt right and always run
        //the value through a function to set it to Network Byte Order..Big Endian)
        addr.sin6_port = htons(port); // byte order is significant

        addr.sin6_addr = in6addr_any; // listen to all interfaces


        /*BIND*/
        /* As its name implies, this system call asks the OS to bind the socket to
         * address and port specified above. */
        retval = bind(server_sock, (struct sockaddr*)&addr, sizeof(addr));
        if(retval < 0) {
            perror("Error binding to port");
            exit(1);
        }

        //connect(server_sock, addr);

        /* Now that we've bound to an address and port, we tell the OS that we're
         * ready to start listening for client connections.  This effectively
         * activates the server socket.  BACKLOG (#defined above) tells the OS how
         * much space to reserve for incoming connections that have not yet been
         * accepted. */

        /*LISTEN*/
        retval = listen(server_sock, BACKLOG);
        if(retval < 0) {
            perror("Error listening for connections");
            exit(1);
        }

        while(1) {
            /* Declare a socket for the client connection. */
            int sock;
            char buffer[256];

            /* Another address structure.  This time, the system will automatically
             * fill it in, when we accept a connection, to tell us where the
             * connection came from. */
            struct sockaddr_in remote_addr;
            unsigned int socklen = sizeof(remote_addr); 

            /* Accept the first waiting connection from the server socket and
             * populate the address information.  The result (sock) is a socket
             * descriptor for the conversation with the newly connected client.  If
             * there are no pending connections in the back log, this function will
             * block indefinitely while waiting for a client connection to be made.
             * */
            printf("Waiting for a new connection...\n");
            //  server_sock is the listen()ing socket descriptor
            //  NOTE: info about incoming connection goes in struct remote_addr is pointing to
            sock = accept(server_sock, (struct sockaddr*) &remote_addr, &socklen);
            if(sock < 0) {
                perror("Error accepting connection");
                exit(1);
            }
            printf("Established connection %d\n", sock);

            //  at this point were ready to communicate on new socket descriptor "sock"
            /*
            int get_info = -1;
            get_info = inet_pton(AF_INET, AMAZON_IP, &addr.sin6_addr.s6_addr);
            assert(get_info == 1);

            ssize_t nByte = send(server_sock, GET_INFO, strlen(GET_INFO), 0);
            if(nByte <= 0){
                perror("send");
                exit(EXIT_FAILURE);
            }
            
            char BUF[BUFSIZ];
            size_t recived_len = 0;
            while ((recived_len = recv(server_sock, BUF, BUFSIZ-1, 0)) > 0)
            {
                int statu;
                BUF[recived_len] = '\0';
                printf("%s", BUF);
                statu = (recived_len==BUFSIZ-1) ? 0 : 1;
                //if (statu) break;
            }
            if (recived_len < 0)
            {
                perror("recv");
            }*/



            //  pass command off to a thread 
            // pthread_t *thread = malloc(sizeof(pthread_t));
            //if(pthread_create(thread, 0, echo, (void*)&sock)){
            //  perror("unable to create thread");
            //exit(1);
            //}
            //struct stat file_stat;
            char *directory = argv[2];
            //if(stat(directory, &file_stat) == -1){
            //   perror("error in stat\n");
            //}   
                    //if(S_ISDIR(file_stat.st_mode) == 0){ //check if it is a directory
                    //     printf("This is a directory!\n");
                    //    handleRequest(sock, directory);
                    //}   
                    //else printf("This is not a directory\n");
                    chdir(directory);
                    /*Generate the response body*/
                    //sprintf(content, "Welcome to Server");
                    //printf("%s", content);
                    handleRequest(sock);

                    /* At this point, you have a connected socket (named sock) that you can
                     * use to send() and recv(). */

                    /* ALWAYS check the return value of send().  Also, don't hardcode
                     * values.  This is just an example.  Do as I say, not as I do, etc. */
                    //)send(sock, "Hello, client.\n", 15, 0);
                    //recv(sock,buffer,255,0);

                    /* Tell the OS to clean up the resources associated with that client
                     * connection, now that we're done with it. */
                    close(sock);
        }

        close(server_sock);
    }
