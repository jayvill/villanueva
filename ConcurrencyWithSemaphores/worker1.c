#include<stdlib.h>
#include<stdio.h>
#include<semaphore.h>

sem_t *service_sem, *request_sem, *last, *mutex, *mutex_2;
void pair();// for every 2 calls to pair() there is one worker and one browser

void init() {
    service_sem = (sem_t*)malloc(sizeof(sem_t));
    request_sem = (sem_t*)malloc(sizeof(sem_t));
    last = (sem_t*)malloc(sizeof(sem_t));
    mutex = (sem_t*)malloc(sizeof(sem_t));
    mutex_2 = (sem_t*)malloc(sizeof(sem_t));
    sem_init(service_sem,0, 0);
    sem_init(request_sem,0, 0);
    sem_init(last, 0, 0);
    sem_init(mutex_2, 0, 1);
    sem_init(mutex, 0, 1);
    //sem_init(final, 0, 0);
}

void wait_for_service() {
	// call pair() when ready
    sem_wait(service_sem);
    sem_post(request_sem);
    sem_wait(mutex_2);
    sem_wait(last);
    sem_post(mutex_2);
    pair();

}

void wait_for_request() {
	// call pair() when ready
    sem_post(service_sem);
    sem_wait(request_sem);
    sem_wait(mutex);
    sem_post(last);
    pair();
    sem_post(mutex);
    //pair();
}
