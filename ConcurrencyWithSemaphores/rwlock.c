#include"rwlock.h"
#include<stdlib.h>
#include<stdio.h>
#include<semaphore.h>
/*
  rwlock_init(rwlock *m)
*/
void rwlock_init(rwlock* m, int concurrent_readers){
    m->mutex = (sem_t*)malloc(sizeof(sem_t));
    m->writers = (sem_t*)malloc(sizeof(sem_t));
    m->readers = (sem_t*)malloc(sizeof(sem_t));
    sem_init(m->mutex,0,1);
    sem_init(m->writers,0,1);
    sem_init(m->readers,0,concurrent_readers);
}
/*
  rlock(rwlock *m)
*/
void rlock(rwlock * m){
    sem_wait(m->writers);
    sem_post(m->readers);
    sem_post(m->writers);
    int sem_num = 0;
    sem_getvalue(m->mutex , &sem_num);
    if (sem_num == 1)
        sem_wait(m->mutex);
}
/*
  runlock(rwlock *m)
*/
void runlock(rwlock * m){
    sem_wait(m->readers);
    int sem_num = 0;
    sem_getvalue(m->readers, &sem_num);

    if(sem_num = 1){
        sem_post(m->mutex);
    }

    sem_getvalue(m->mutex, &sem_num);
    if (sem_num > 1){
        sem_wait(m->mutex);
    }
}
/*
  wlock(rwlock *m)
*/
void wlock(rwlock * m){
    sem_wait(m->mutex);
    sem_wait(m->writers);
}
/*
  wunlock(rwlock *m)
*/
void wunlock(rwlock * m){
    sem_post(m->readers);
    sem_post(m->writers);
    sem_post(m->mutex);
}
