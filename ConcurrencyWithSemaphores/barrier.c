#include"barrier.h"
#include<stdlib.h>
#include<stdio.h>
#include<semaphore.h>
/*
void P(sem_t *s) //wrapper function for sem_wait
void V(sem_t *s) //wrapper function for sem_post

 typedef struct {
    sem_t *mutex, *enter, *exit;
    int size;
    int count;
  
  } barrier;

 initalize a barrier for use
 */
void barrier_init(barrier* b, int s) {
    b->size = s;
    b->count = 0;
   // sem_init(b->mutex, 0, 1);
   // sem_init(b->enter, 0, 0);
   // sem_init(b->exit, 0, 0);
    b->mutex = malloc(sizeof(barrier));
    b->enter = malloc(sizeof(barrier));
    b->exit = malloc(sizeof(barrier));
    sem_init(b->mutex, 0, 1);
    sem_init(b->enter, 0, 0);
    sem_init(b->exit, 0, 0);
}

void barrier_wait(barrier* b) {
  // phase1(b);
  // phase2(b); 
    int i = 0;
    sem_wait(b->mutex);
    b->count += 1;
    if(b->count == b->size)
        while(i < b->size){
            sem_post(b->enter);
            i++;
        }
    sem_post(b->mutex);
    sem_wait(b->enter);
    int j = 0;
    sem_wait(b->mutex);
    b->count -= 1;
    if(b->count == 0)
        while(j < b->size){
            sem_post(b->exit);
            j++;
        }
    sem_post(b->mutex);
    sem_wait(b->exit);
}
void phase1(barrier* b){
    int i = 0;
    sem_wait(b->mutex);
    b->count += 1;
    if(b->count == b->size)
        while(i < b->size){
            sem_post(b->enter);
            i++;
        }
    sem_post(b->mutex);
    sem_wait(b->enter);
}
void phase2(barrier* b){
    int j = 0;
    sem_wait(b->mutex);
    b->count -= 1;
    if(b->count == 0)
        while(j < b->size){
            sem_post(b->exit);
            j++;
        }
    sem_post(b->mutex);
    sem_wait(b->exit);
}
