#include"mutex.h"
#include<stdlib.h>
#include<stdio.h>

void mutex_init(mutex* m) {
    //int num;
   m->s = malloc(sizeof(mutex));
    sem_init(m->s, 0, 1);
}
void mutex_lock(mutex* m) {
    sem_wait(m->s);
    //sem_trywait(m->s);
}
void mutex_unlock(mutex* m) {
   sem_post(m->s);
   
}
