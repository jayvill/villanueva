package mp4;

interface Inventory {
	public Product[] GenerateShoppingList();
	public void AddProduct(Condiment c);
	public void ListInventory();
}
