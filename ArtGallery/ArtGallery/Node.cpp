//
//  Node.cpp
//  ArtGallery
//
//  Created by Jonathan Villanueva on 11/12/15.
//  Copyright (c) 2015 Jonathan Villanueva. All rights reserved.
//

#include "Node.h"
#include "Painting_Wrapper.h"

//constructor
Node::Node(void){
    this->next = NULL;//a newly created pointer points to nothing
    //this->painting = Painting();//call default constructor in Painting.cpp
    
    this->painting_wrapper = Painting_Wrapper();// create a painting wrapper with default constructor
}

//destructor
Node::~Node(void){
    //deallocate the memory that was previously reserved
}
Painting_Wrapper Node::getPaintingWrapper(void){
    return painting_wrapper;
}

Node Node::getNext(void){
    return *this->next;
}
string Node::getTitle(){
    return (this->painting_wrapper)->Title;
}