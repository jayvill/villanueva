//
//  Painting.cpp
//  ArtGallery
//
//  Created by Jonathan Villanueva on 11/12/15.
//  Copyright (c) 2015 Jonathan Villanueva. All rights reserved.
//

#include "Painting.h"
Painting::Painting() {
    this->Title = "no title";
	this->ArtistName = "no artist name";
	this->Image = 0;
	this->ID = 0;
}

Painting::Painting(int _ID, string _Title, string _ArtistName, int _image){
	this->ID = _ID;
	this->Title = _Title;
	this->ArtistName = _ArtistName;
	this->Image = _image;
}

Painting::~Painting(){

}

//Painting Painting::getPainting(){
 //   return this->Painting;
//}

string Painting::getTitle(){
    return this->Title;
}

