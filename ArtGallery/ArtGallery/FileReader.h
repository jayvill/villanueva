//
//  FileReader.h
//  ArtGallery
//
//  Created by Jonathan Villanueva on 11/14/15.
//  Copyright (c) 2015 Jonathan Villanueva. All rights reserved.
//

#ifndef __ArtGallery__FileReader__
#define __ArtGallery__FileReader__

#include <iostream>
//#include <vector>
#include <string>
#include <fstream>
#include "Painting_Wrapper.h"
using namespace std;
class FileReader
{
public:
    string* input_values;
    //vector<string> parseFile(string path);
    string* parse_File(string path);
    void parse_File_(Painting_Wrapper &wrapper, string path);
    //void writeToFile(Painting_Wrapper&, string path);
    void writeToFile(string path);
    void write_ToFile(Painting_Wrapper&, string path);
   // FileReader();
    ~FileReader();
};
#endif /* defined(__ArtGallery__FileReader__) */
