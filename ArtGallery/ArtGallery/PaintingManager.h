//
//  PaintingWrapper.h
//  ArtGallery
//
//  Created by Jonathan Villanueva on 11/12/15.
//  Copyright (c) 2015 Jonathan Villanueva. All rights reserved.
//

#ifndef __ArtGallery__PaintingManager__
#define __ArtGallery__PaintingManager__

#include <iostream>

#include "Artist.h"
#include "Painting.h"
#include "Painting_Wrapper.h"
#include "FileReader.h"
#include <stdio.h>
#include <fstream>

class PaintingManager {
    private:
        Painting a_painting;
        LinkedList *_list;
        unsigned int numPaintings;
        Painting *pointer;
        Painting_Wrapper pw;
        void createWrappers();
        void loadAll();
        Painting_Wrapper *pw1;
        Painting_Wrapper *pw2;
        Painting_Wrapper *pw3;
        Painting_Wrapper *pw4;
        Painting_Wrapper *pw5;
    public:
        PaintingManager();
        virtual ~PaintingManager();
        bool addPainting(LinkedList list, Painting painting);
        void initArtGallery();
        void function_l();
        int editPainting(int x);
        void function_t(int _id, char* title);
        void function_a(int _id, char* author);
        void saveToDisk();
        void quit();
};

#endif /* defined(__ArtGallery__PaintingWrapper__) */
