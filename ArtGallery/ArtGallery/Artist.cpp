//
//  Artist.cpp
//  ArtGallery
//
//  Created by Jonathan Villanueva on 11/14/15.
//  Copyright (c) 2015 Jonathan Villanueva. All rights reserved.
//

#include "Artist.h"

Artist::Artist(){
    firstName = "no first name";
    lastName = "no last name";
}

Artist::Artist(string first_name, string last_name){
    firstName = first_name;
    lastName = last_name;
}