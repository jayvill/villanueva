//
//  main.cpp
//  ArtGallery
//
//  Created by Jonathan Villanueva on 11/12/15.
//  Copyright (c) 2015 Jonathan Villanueva. All rights reserved.
//
#include <iostream>
//#include <fstream>
//#include <string>
#include <stdio.h>
#include <stdlib.h>
//#include <fstream>
//#include <vector>
//#include "FileReader.h"
#include "PaintingManager.h"

using namespace std;

/*vector<string> parseFile(string path)
{
    vector<string> storedValues;
    ifstream myfile(path);
    copy(istream_iterator<string>(myfile), istream_iterator<string>(),back_inserter(storedValues));
    return storedValues;
}*/

int main()
{
	char input[1];
    char title[256];
    char author[256];
    //LinkedList* _list = new LinkedList(5); //calls default constructor
    //vector<string> p1, p2, p3, p4, p5;
    //Node *currentPainting;
    //string current;
    
    //parse data from the .txt input files
    //p1 = parseFile("/Users/rcnblck/UIC_COURSES/CS_FOLDER/474/ArtGallery/ArtGallery/Painting1.txt");
    //p2 = parseFile("/Users/rcnblck/UIC_COURSES/CS_FOLDER/474/ArtGallery/ArtGallery/Painting2.txt");
    //p3 = parseFile("/Users/rcnblck/UIC_COURSES/CS_FOLDER/474/ArtGallery/ArtGallery/Painting3.txt");
   // p4 = parseFile("/Users/rcnblck/UIC_COURSES/CS_FOLDER/474/ArtGallery/ArtGallery/Painting4.txt");
   // p5 = parseFile("/Users/rcnblck/UIC_COURSES/CS_FOLDER/474/ArtGallery/ArtGallery/Painting5.txt");
    
    //create 5 Paintings and store the relevant data in them
    //DECLARED ON THE STACK
    //destructor called automatically because it is declared on the stack
    /*Painting pic1 (stoi(p1[0]), p1[1], p1[2], stoi(p1[3]));
    Painting pic2 (stoi(p2[0]), p2[1], p2[2], stoi(p2[3]));
    Painting pic3 (stoi(p3[0]), p3[1], p3[2], stoi(p3[3]));
    Painting pic4 (stoi(p4[0]), p4[1], p4[2], stoi(p4[3]));
    Painting pic5 (stoi(p5[0]), p5[1], p5[2], stoi(p5[3]));*/
    
    PaintingManager *art_gallery;
    art_gallery = new PaintingManager();
    art_gallery->initArtGallery();
    int editItem = 0;
    cout << "Welcome to the Art Gallery!" << endl;
    do
	{
		 cout << "l - List all paintings." << endl;
         cout << "1 .. 5 - Edit painting" << endl;
         cout << "t - Change title of current painting" << endl;
         cout << "a - Change the name of the author of the current painting" << endl;
         cout << "s - Save all paintings" << endl;
         cout << "q - QUIT" << endl;
        
        cin >> input;
		switch(input[0]){
			case 'l':
				//_list->displayList(_list->head);
                art_gallery->function_l();
                break;
			case '1':
                //currentPainting = _list->listAt(1);
                //current = _list->getTitle(currentPainting);
                //cout<<"current painting is"+ current + '\n'<< endl;
                editItem = art_gallery->editPainting(1);
                cout<<"currently editing painting 1"<<endl;
				break;
			case '2':
                editItem = art_gallery->editPainting(2);
                cout<<"currently editing painting 2"<<endl;
				break;
			case '3':
                editItem = art_gallery->editPainting(3);
                cout<<"currently editing painting 3"<<endl;
				break;
			case '4':
                editItem = art_gallery->editPainting(4);
                cout<<"currently editing painting 4"<<endl;
				break;
			case '5':
				editItem = art_gallery->editPainting(5);
                cout<<"currently editing painting 5"<<endl;
                break;
			case 't':
                if(editItem == 0){
                    cout<<"please choose a painting to edit first"<<endl;
                }
                else{
                    cout<<"What would you like to change the title to?"<<endl;
                    cin >> title;
                    art_gallery->function_t(editItem, title);
                }
                break;
			case 'a':
                if(editItem == 0){
                    cout<<"please choose a painting to edit first"<<endl;
                }
                else{
                    cout<<"What would you like to change the artist to?"<<endl;
                    cin >> author;
                    art_gallery->function_a(editItem, author);
                }
				break;
			case 's':
                if(editItem == 0){
                    cout<<"please choose a painting to edit first"<<endl;
                }
                else{
                    cout<<"Saving changes..."<<endl;
                    art_gallery->saveToDisk();
                    cout<<"Done"<<endl;
                }
				break;
			case 'q':
                art_gallery->quit();
                delete(art_gallery);
                cout << "Quitting Art Gallery..." << endl;
				break;
            case 'Q':
                art_gallery->quit();
                delete(art_gallery);
                cout << "Quitting Art Gallery..." << endl;
                break;
			default:
				break;
		}
	}while(!((input[0] == 'Q')||(input[0] == 'q')));

	return 0;
}
