//
//  LinkedList.cpp
//  ArtGallery
//
//  Created by Jonathan Villanueva on 11/12/15.
//  Copyright (c) 2015 Jonathan Villanueva. All rights reserved.
//

#include "LinkedList.h"
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <iostream>
#include "Node.h"


using namespace std;

LinkedList::LinkedList() {//create an empty linked list
	//dynamically allocate space by creating a Node and setting it to head
    this->head = NULL;
    this->tail = NULL;;
}

LinkedList::LinkedList(int size){
    this->head = NULL;
    this->tail = NULL;
    this->size = size;
}

LinkedList::~LinkedList() {
	// TODO Auto-generated destructor stub
}

//getters and setters
void LinkedList::setHead(Node x){
    this->head = &x;
}

Node LinkedList::getHead(){
    return *head;
}

void LinkedList::setTail(Node x){
    this->tail = &x;
}

Node LinkedList::getTail(){
    return *tail;
}

bool LinkedList::isEmpty(Node *head){//check if list is empty
    if (head == NULL)
        return true;
    else
        return false;
}

void LinkedList::insertLast(Node *&head, Node *&tail, Painting_Wrapper painting_wrapper){//insert at tail
    Node *temp = new Node();
    
    if (isEmpty(head)){ //check if the list is empty
        temp->painting_wrapper = painting_wrapper;
        temp->next = NULL;
        head = temp;
        tail = temp;
    }
    else{
        temp->painting_wrapper = painting_wrapper;
        temp->next = NULL;
        tail->next = temp;
        tail = tail->next;
    }

}
void LinkedList::addNode(Node *&head, Node *&tail, Painting_Wrapper pw){//insert at head
    Node *temp = new Node;
    
    if (isEmpty(head)){
        temp->painting_wrapper = pw;
        temp->next = NULL;
        head = temp;
        tail = temp;
    }
    else{
        temp->painting_wrapper = pw;
        tail->next = temp;
        temp->next = NULL;
        tail = temp;
    }
}
void LinkedList::removeFirst(Node *&head, Node *&tail){//remove the first node
    if (isEmpty(head)){
        cout << "Can't delete from an empty list\n" << endl;
    }
    else if(head->next == NULL){ //if head is the only node then delete it
        //delete head->painting_wrapper;
        delete head;
        cout<<"deleted final node"<<endl;
        head = NULL;
        cout<<"set head to NULL"<<endl;
        tail = NULL;
        cout<<"set tail to NULL"<<endl;
    }
    else{ //remove current head and set the new head
        Node *temp = head;
        head = head->next;
        delete temp;
        cout<<"deleted current head and set new head";
    }

}
void LinkedList::removeLast (Node *&head, Node *&tail){//remove the last node
    if (isEmpty(head)){
        cout << "Can't delete from an empty list\n" << endl;
    }
    else if(head->next == NULL){
        delete head;
        head = NULL;
        tail = NULL;
    }
    else{
        Node *nextToLast = head;
        Node *last = head->next;
        
        while(last->next != NULL){
            nextToLast = last;
            last = last->next;
        }
        nextToLast->next = NULL;
        tail = nextToLast;
        delete last;
    }
}
void LinkedList::displayList(Node *head){//display the list
    Node *temp = head;
    
    if (isEmpty(head)){
        cout << "Can't display an empty list\n" << endl;
    }
    else{

        cout<<"\nList contains:"<<endl<<endl;
        while(temp != NULL){
            //cout<<"ID: "<<painting.ID<<endl;
            cout<<"ID: "<<temp->painting_wrapper.aPainting_ptr->ID<<endl;
            cout<<"Title: "<<temp->painting_wrapper.aPainting_ptr->Title<<endl;
            cout<<"Artist Name: "<<temp->painting_wrapper.aPainting_ptr->ArtistName<<endl;
            cout<<"Image:"<<temp->painting_wrapper.aPainting_ptr->Image<<endl<<endl;
            temp = temp->next;
        }
    }
}

Node* LinkedList:: listAt(int x){
    int cntr = 1;
    Node* pointer;
    pointer = this->head;
    if(x == 1)
        return pointer;
    while(cntr != x)
    {
        if(pointer == NULL)
            return NULL;
        else
        {
            pointer = pointer->next;
            cntr++;
        }
    }
    return pointer;
}

string LinkedList::getTitle(Node* node){
    return node->getTitle();
}
