//
//  Node.h
//  ArtGallery
//
//  Created by Jonathan Villanueva on 11/12/15.
//  Copyright (c) 2015 Jonathan Villanueva. All rights reserved.
//

#ifndef __ArtGallery__Node__
#define __ArtGallery__Node__

#include <iostream>
#include "Painting.h"
#include "Painting_Wrapper.h"
class Painting_Wrapper;

class Node{
    friend class LinkedList;
public:
    //instance variables
    Node* next;
    //Painting painting;
    Painting_Wrapper painting_wrapper;
    //functions
    Node();
    //virtual
    ~Node();
    Painting_Wrapper getPaintingWrapper();
    Node getNext();
    string getTitle();
};

#endif /* defined(__ArtGallery__Node__) */
