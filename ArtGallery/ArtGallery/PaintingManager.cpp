//
//  PaintingWrapper.cpp
//  ArtGallery
//
//  Created by Jonathan Villanueva on 11/12/15.
//  Copyright (c) 2015 Jonathan Villanueva. All rights reserved.
//

#include "PaintingManager.h"
#include "Node.h"
//#include <memory>
static int inMemCount = 0;

PaintingManager::PaintingManager() {
	// TODO Auto-generated constructor stub
    _list = new LinkedList(5); //initalize a linked list using of size 5
    numPaintings = 5;
    
}

PaintingManager::~PaintingManager(){

}

void PaintingManager::initArtGallery(){
    //PaintingManager();//linked list created
    createWrappers();//linked list populated
}

void PaintingManager::createWrappers(){
    
    //put the wrappers in the heap
    pw1 = new Painting_Wrapper(1);
    pw2 = new Painting_Wrapper(2);
    pw3 = new Painting_Wrapper(3);
    pw4 = new Painting_Wrapper(4);
    pw5 = new Painting_Wrapper(5);
    
    _list->addNode(_list->head, _list->tail, *pw1);
    _list->addNode(_list->head, _list->tail, *pw2);
    _list->addNode(_list->head, _list->tail, *pw3);
    _list->addNode(_list->head, _list->tail, *pw4);
    _list->addNode(_list->head, _list->tail, *pw5);
}

void PaintingManager::function_l(){
    loadAll();
    //_list->displayList(_list->head);
    //pw.un_initalizeWrapper(Painting_Wrapper &rapper);
}

void PaintingManager::loadAll(){
    Node* item1 = _list->listAt(1);
    Node* item2 = _list->listAt(2);
    Node* item3 = _list->listAt(3);
    Node* item4 = _list->listAt(4);
    Node* item5 = _list->listAt(5);
    
    //product->scheme->edit scheme->use custom working directory
    pw.initalize_Wrapper(item1->painting_wrapper,"Painting1.txt");
    inMemCount+=1;
    pw.initalize_Wrapper(item2->painting_wrapper,"Painting2.txt");
    inMemCount+=1;
    pw.initalize_Wrapper(item3->painting_wrapper,"Painting3.txt");
    inMemCount+=1;
    pw.initalize_Wrapper(item4->painting_wrapper,"Painting4.txt");
    inMemCount+=1;
    pw.initalize_Wrapper(item5->painting_wrapper,"Painting5.txt");
    inMemCount+=1;
    //cout<<item1->painting_wrapper._id<<endl;
    //cout<<item2->painting_wrapper._id<<endl;
    //cout<<item3->painting_wrapper._id<<endl;
    //cout<<item4->painting_wrapper._id<<endl;
    //cout<<item5->painting_wrapper._id<<endl;

    _list->displayList(_list->head);
    
    //wrappers 1 and 2 initally stay in memory
    //wrappers 3, 4, and 5
    pw.un_initalizeWrapper(item3->painting_wrapper);
    inMemCount-=1;
    pw.un_initalizeWrapper(item4->painting_wrapper);
    inMemCount-=1;
    pw.un_initalizeWrapper(item5->painting_wrapper);
    inMemCount-=1;
    //_list->displayList(_list->head);//USED HERE FOR DEBUGGING
    
    //pw.initalizeWrapper(item2->painting_wrapper,"/Users/rcnblck/UIC_COURSES/CS_FOLDER/474/ArtGallery/ArtGallery/Painting2.txt");
    //pw.initalizeWrapper(item3->painting_wrapper,"/Users/rcnblck/UIC_COURSES/CS_FOLDER/474/ArtGallery/ArtGallery/Painting3.txt");
    //pw.initalizeWrapper(item4->painting_wrapper,"/Users/rcnblck/UIC_COURSES/CS_FOLDER/474/ArtGallery/ArtGallery/Painting4.txt");
    //pw.initalizeWrapper(item5->painting_wrapper,"/Users/rcnblck/UIC_COURSES/CS_FOLDER/474/ArtGallery/ArtGallery/Painting5.txt");

}
int PaintingManager::editPainting(int x)
{
    FileReader fr;
    
    //set the current painting to x to be edited
    Node* itemToEdit = _list->listAt(x);

    //obtain correct path
    string path;
    //if(x==1 || x==2 || x==3 || x==4 || x==5)
      //  path = itemToEdit->painting_wrapper.path;
    if(x == 1)
        path = "Painting1.txt";
    if(x == 2)
        path = "Painting2.txt";
    if(x == 3)
        path = "Painting3.txt";
    if(x == 4)
        path = "Painting4.txt";
    if(x == 5)
        path = "Painting5.txt";
    //check if painting is in memory
    if(itemToEdit->painting_wrapper.operator->() == NULL)//if not in memory check how many paintings are currently in memory
    {
        //if the memory is full, un_initalize a painting to make room
        if(inMemCount >= 2)
        {
            //find a painting that is in memory and remove it from memory
            Node* temp = _list->head;
            while(temp != NULL){
                if(temp->painting_wrapper.operator->() != NULL){
                    
                    //save changes out to disk before removing it from memory
                    fr.write_ToFile(temp->painting_wrapper, path);
                    cout<<"saved memory to disk before swapping"<<endl;
                    
                    //remove from memory
                    pw.un_initalizeWrapper(temp->painting_wrapper);
                    inMemCount-=1;
                    pw.initalize_Wrapper(itemToEdit->painting_wrapper,path);
                    inMemCount+=1;
                    break;
                }
                else
                    temp = temp->next;
            }
        }
        //if memory is not full then initalize the painting
        if(inMemCount<2)
        {
            pw.initalize_Wrapper(itemToEdit->painting_wrapper,path);
            inMemCount+=1;
        }
        cout<< "current cache usage = "<<inMemCount<<endl;
        //return painting wrapper ID of current painting to edit
        return itemToEdit->painting_wrapper._id;
    }
     //if not in memory check how many paintings are currently in memory
    else{
        cout<< "current cache usage = "<<inMemCount<<endl;
        return itemToEdit->painting_wrapper._id;
    }
}
//use the returned id from editPainting as parameter
void PaintingManager::function_t(int _id, char* title){
    //get the current painting and change its title
    Node* toEdit = _list->listAt(_id);
    //Painting p = toEdit->painting_wrapper.operator*();
    Painting *p = toEdit->painting_wrapper.operator->();
    string oldTitle = p->Title;
    //p.Title = title;
    //cout<<"changed title name from "<<oldTitle<<" to "<<p.Title<<endl;
    p->Title = title;
    cout<<"changed title name from "<<oldTitle<<" to "<<p->Title<<endl;
}
void PaintingManager::function_a(int _id, char* author){
    //get the current painting and change its author
    Node* toEdit = _list->listAt(_id);
    //Painting p = toEdit->painting_wrapper.operator*();
    Painting *p = toEdit->painting_wrapper.operator->();
    string oldArtist = p->ArtistName;
    //p.ArtistName = author;
    //cout<<"changed artist name from "<<oldArtist<<" to "<<p.ArtistName<<endl;
    p->ArtistName = author;
    cout<<"changed artist name from "<<oldArtist<<" to "<<p->ArtistName<<endl;
}
void PaintingManager::saveToDisk(){
    char newfile[256];
    //find the paintings that are in memory, delete corresponding file, replace it with new edited file
    Node* temp = _list->head;
    while(temp != NULL){
        //if a wrappers painting is not null then write its contents to disk(a file)
        if(temp->painting_wrapper.operator->() != NULL)
        {
            Painting p = temp->painting_wrapper.operator*();
            
            //convert string path to char array
            string tmp = temp->painting_wrapper.path;
            char path[1024];
            strncpy(path, tmp.c_str(), sizeof(path));
            path[sizeof(path) - 1] = 0; // null terminate char array
            
            //delete associated path
            if( remove(path) != 0 )
                perror( "Error deleting file" );
            else
                cout<< "Old file successfully deleted"<<endl;
            //create new path write changes and set wrappers path to the new path
            cout<<"saving contents to file. Give the new file a name <filename>.<txt>\n"<<endl;
            cin>>newfile;
            //newfile = newfile+".txt";
            ofstream a_file(newfile);
            a_file<<p.ID<<"\n";
            a_file<<p.Title<<"\n";
            a_file<<p.ArtistName<<"\n";
            a_file<<p.Image<<"\n";
            temp->painting_wrapper.changePath(newfile);
            temp = temp->next;
        }
        else
            temp = temp->next;
    }
}
void PaintingManager::quit(){
    //delete paintings
    Node* temp = _list->head;
    while(temp != NULL)
    {
        if(temp->painting_wrapper.aPainting_ptr != NULL){
            cout<<"deleting painting"<<endl;
            delete(temp->painting_wrapper.aPainting_ptr);
            cout<<"setting wrapper pointer to null"<<endl;
            temp->painting_wrapper.aPainting_ptr = NULL;
        }
        temp = temp->next;
    }
    
    //delete wrappers
    cout<<"deleting wrapper 1"<<endl;
    delete(pw1);
    cout<<"setting wrapper 1 pointer to NULL"<<endl;
    pw1 = NULL;
    
    cout<<"deleting wrapper 2"<<endl;
    delete(pw2);
    cout<<"setting wrapper 2 pointer to NULL"<<endl;
    pw2 = NULL;
    
    cout<<"deleting wrapper 3"<<endl;
    delete(pw3);
    pw3 = NULL;
    cout<<"setting wrapper 3 pointer to NULL"<<endl;
    
    cout<<"deleting wrapper 4"<<endl;
    delete(pw4);
    pw4 = NULL;
    cout<<"setting wrapper 4 pointer to NULL"<<endl;
    
    cout<<"deleting wrapper 5"<<endl;
    delete(pw5);
    pw5 = NULL;
    cout<<"setting wrapper 5 pointer to NULL"<<endl;
    
    //delete linked list
    while(_list->head!=NULL){
        _list->removeFirst(_list->head, _list->tail);
    }
    _list = NULL;
    cout<<"list pointer is null"<<endl;
}

