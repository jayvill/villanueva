//
//  Painting.h
//  ArtGallery
//
//  Created by Jonathan Villanueva on 11/12/15.
//  Copyright (c) 2015 Jonathan Villanueva. All rights reserved.
//

#ifndef __ArtGallery__Painting__
#define __ArtGallery__Painting__

#include <iostream>
#include <string>
using namespace std;

class Painting {
public:
	string Title;
	string ArtistName;
	int Image;
	int ID;
	Painting(); //default constructor
	//virtual
    ~Painting(); //destructor
	Painting(int ID, string Title, string ArtistName, int image);
    string getTitle();
    Painting getPainting();
private:
    
};
#endif /* defined(__ArtGallery__Painting__) */
