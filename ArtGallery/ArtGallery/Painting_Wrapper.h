//
//  Painting_Wrapper.h
//  ArtGallery
//
//  Created by Jonathan Villanueva on 11/15/15.
//  Copyright (c) 2015 Jonathan Villanueva. All rights reserved.
//

#ifndef __ArtGallery__Painting_Wrapper__
#define __ArtGallery__Painting_Wrapper__

#include <iostream>
#include "Painting.h"
#include "LinkedList.h"//the cache

using namespace std;

class Painting;

class ReferenceCounter{
private:
    int counter;
public:
    ReferenceCounter(){
        counter = 0;
    }
    void increase(){
        counter++;
    }
    int decrease(){
        --counter;
        if(counter < 0){
            cout<<"ReferenceCounter is < 0"<<endl;
        }
        return counter;
    }
    int getCount(){
        return counter;
    }
};

class Painting_Wrapper{
public:
    //the key used to access the object in the file if necessary
    int _id;
    //the local copy of the persistent Painting object used in this application
    //(NULL id the object is swapped out or has not been accessed)
    Painting* aPainting_ptr;
    string path;
    ReferenceCounter* dataReferenceCounter;
    Painting_Wrapper(int);
    Painting_Wrapper();
    Painting_Wrapper(int x,Painting &aPainting);
    ~Painting_Wrapper();

    Painting* operator->()const{
        return aPainting_ptr;
        //Object *b = new Object()
        //b->
        //Pointer<Object> c(new Object());
        //c->

    }
    Painting& operator*()const{
        return *aPainting_ptr;
    }
    void setID(int);
    //void initalizeAllWrappers();
    //void initalizeWrapper(Painting_Wrapper, string);
    //void initalizeWrapper(Painting*&, string);
    //void initalizeWrapper(Painting_Wrapper &rapper, string path);
    void un_initalizeWrapper(Painting_Wrapper &rapper);
    void initalizeWrapper(Painting*, string);
    
    void initalize_Wrapper(Painting_Wrapper &rapper, string path);
    void initalizeWrappers();
    Painting* getPainting();
    void changePath(char* path);
private:
    
protected:
    
};






/*template<class T>
//wrapper class for the Painting clas. Behaves as a smart pointer
class Painting_Wrapper{
    
    //hold an instance of Painting
    T* data;
    ReferenceCounter* dataReferenceCounter;

public:
    Painting_Wrapper() : data(NULL), dataReferenceCounter(NULL){
        dataReferenceCounter = new ReferenceCounter();
        dataReferenceCounter->increase();
    }
    //get a handle to a painting object
    Painting_Wrapper(T* xRefernce): data(xRefernce), dataReferenceCounter(NULL){
        dataReferenceCounter = new ReferenceCounter();
        dataReferenceCounter->increase();
    }
    //copy a preexisting smart pointer and increase the number of refereces to it
    Painting_Wrapper(const Painting_Wrapper<T> &copy):data(copy.data), dataReferenceCounter(copy.dataReferenceCounter){
        dataReferenceCounter->increase();
    }
    //finalize(i.e, write the Painting object back to the file if necessary)
    virtual ~Painting_Wrapper(){
        if(dataReferenceCounter->decrease() == 0){
            delete data;
            delete dataReferenceCounter;
            cout<<"memory cleaned up"<<endl;
        }
    }*/
    /*
        ASSIGNMENT OPERATOR
        
        used for when we want:
        smartPtr A = object
        smartPtr B = A;
     
     */
    /*
    Painting_Wrapper<T>& operator = (const Painting_Wrapper<T> &other){
        //prevent self assignment
        if(this != &other){
            if(dataReferenceCounter->decrease()==0){
                delete data;
                delete dataReferenceCounter;
            }
            data = other.data;
            dataReferenceCounter = other.dataReferenceCounter;
            dataReferenceCounter->increase();
        }
        return *this;//return actual object
    }
    T& operator*(){
        return *data;
    }
    T* operator->(){
        return data;
        //Object *b = new Object()
        //b->
        //Pointer<Object> c(new Object());
        //c->
    }
    
    //get the object
    Painting* getData();
    //send a message to the Painting object handled by the reciever
    Painting* operator->()const;
    //obtain a reference to the Painting object handled by the receiver
    Painting& operator*()const;
protected:
    //the key used to access the object in the file if necessary
    int _id;
    //the local copy of the persistent Painting object used in this application
    //(NULL id the object is swapped out or has not been accessed)
    Painting* aPainting;
    
   // PaintingPtr(const PaintingPtr&);
    //PaintingPtr& operator=(const PaintingPtr&);    

}; */
#endif /* defined(__ArtGallery__PaintingPtr__) */
