//
//  Painting_Wrapper.cpp
//  ArtGallery
//
//  Created by Jonathan Villanueva on 11/15/15.
//  Copyright (c) 2015 Jonathan Villanueva. All rights reserved.
//

#include "Painting_Wrapper.h"
#include "FileReader.h"
#include "LinkedList.h"

using namespace std;

static int inMemCount = 0;

Painting_Wrapper::Painting_Wrapper()
{
    this->_id = 0;
    this->aPainting_ptr = NULL;
    //this->aPainting_ptr = new Painting();
    //this->dataReferenceCounter = NULL;
}

Painting_Wrapper::~Painting_Wrapper(){

}

//inline
Painting_Wrapper::Painting_Wrapper(int x)//: dataReferenceCounter(NULL)
{
    this-> _id = x;
    this->aPainting_ptr = NULL;
    //dataReferenceCounter = new ReferenceCounter();
    //dataReferenceCounter->increase();
}


void Painting_Wrapper::setID(int x){
    _id = x;
}

void Painting_Wrapper::un_initalizeWrapper(Painting_Wrapper &wrapper){
    FileReader fr;
    if(wrapper.aPainting_ptr != NULL){
        string path;
        if(wrapper._id == 1)
            path = "Painting1.txt";
        if(wrapper._id == 2)
            path = "Painting2.txt";
        if(wrapper._id == 3)
            path = "Painting3.txt";
        if(wrapper._id == 4)
            path = "Painting4.txt";
        if(wrapper._id == 5)
            path = "Painting5.txt";
        
        //save changes out to disk
        fr.write_ToFile(wrapper, path);
        
        //delete the painting from memory
        delete(wrapper.aPainting_ptr);
        wrapper.aPainting_ptr = NULL;
        inMemCount--;
    }
    //else{cerr<<"un_initalized a NULL Painting in wrapper"<<endl;}
}

void Painting_Wrapper::initalize_Wrapper(Painting_Wrapper &wrapper, string path){
    //when program starts there is nothing in memory
    //if(inMemCount < 2){
        if(wrapper.aPainting_ptr==NULL){
            wrapper.path = path;
            FileReader fp;
            //vector<string> p1;
            //string* values;
            //values = fp.parse_File(path);
            //wrapper.aPainting_ptr = new Painting(stoi(values[0]), values[1], values[2], stoi(values[3]));
            
            fp.parse_File_(wrapper, path);
            inMemCount++;


            
            
            
        }
  
}
void Painting_Wrapper::changePath(char* path){
    string str(path);
    this->path = str;
}

