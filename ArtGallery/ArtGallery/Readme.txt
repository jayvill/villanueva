compiled in Xcode
To compile the program from a command prompt
	make ArtGallery
	./ArtGallery
I ran the code in valgrind and found a memory leak.
I think this leak happens in FileReader.cpp when parseFile is called.
I couldn't figure out how to properly fix this without crashing the program.
The valgrind log for the l command followed by the quit command is included.
