//
//  FileReader.cpp
//  ArtGallery
//
//  Created by Jonathan Villanueva on 11/14/15.
//  Copyright (c) 2015 Jonathan Villanueva. All rights reserved.
//

#include "FileReader.h"

FileReader::~FileReader(){
    //delete(input_values);
    input_values = NULL;
    //delete(this);
}
string* FileReader::parse_File(/*Painting_Wrapper &wrapper,*/ string path){
    int x = 0;
    ifstream inFile(path);
    string line;
    
    string* input_values = new string[3];
    

    if(inFile.is_open()){
        while(getline(inFile, line)){
            input_values[x] = line;
            x++;
        }
        cout<<"parsed file "<<path<<endl;
        inFile.close();
        //for(int i =0; i<input_values->length();i++){
            
        //}
    }
    else {
        cerr<<"cannot open file " << path << " for input\n"<<endl;
    }
    //delete(input_values);
    return input_values;
}

void FileReader::parse_File_(Painting_Wrapper &wrapper, string path){
    int x = 0;
    ifstream inFile(path);
    string line;
    
    string* input_values = new string[3];

    if(inFile.is_open()){
        while(getline(inFile, line)){
            input_values[x] = line;
            x++;
        }
        cout<<"parsed file "<<path<<endl;
        inFile.close();
        wrapper.aPainting_ptr = new Painting(stoi(input_values[0]), input_values[1], input_values[2], stoi(input_values[3]));
    }
    else {
        cerr<<"cannot open file " << path << " for input\n"<<endl;
    }

}

void FileReader::write_ToFile(Painting_Wrapper &wr, string path){
    ofstream outFile;
    outFile.open(path);
    //write wrappper painting contents to path
    Painting temp;
    temp = wr.operator*();
    outFile << temp.ID<<"\n";
    outFile << temp.Title<<"\n";
    outFile << temp.ArtistName<<"\n";
    outFile << temp.Image<<"\n";
    outFile.close();
}

