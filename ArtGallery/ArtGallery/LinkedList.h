//
//  LinkedList.h
//  ArtGallery
//
//  Created by Jonathan Villanueva on 11/12/15.
//  Copyright (c) 2015 Jonathan Villanueva. All rights reserved.
//

#ifndef __ArtGallery__LinkedList__
#define __ArtGallery__LinkedList__

#include <iostream>
#include <string>
using namespace std;
//#include "Painting_Wrapper.h"
class Node;
class Painting_Wrapper;

class LinkedList {
    /*class Node{
        friend class Node;
    public:
        //instance variables
        Node* next;
        Painting painting;
        //functions
        Node();
        virtual ~Node();
        Painting getPainting();
        Node getNext();
    }*first_node;*/
    
unsigned int size;
    
public:
    //instance variables
    Node* head;
    Node* tail;
    //functions
	LinkedList();//constructor
    LinkedList(int size);
    LinkedList(const LinkedList&);
	
    //virtual
    ~LinkedList(); //destructor
	
    bool isEmpty(Node *head);//check if list is empty
	void insertLast(Node *&head, Node *&tail, Painting_Wrapper painting_wrapper);//insert at tail
	
    void addNode(Node *&head, Node *&tail, Painting_Wrapper painting);//insert at head
	
    void removeFirst (Node *&head, Node *&tail);//remove the first node
	void removeLast (Node *&head, Node *&tail);//remove the last node
	void displayList(Node *head);//display the list
    void deleteAll();
    void editPainting();
    unsigned int getSize();
    string getTitle(Node* node);
    Node* listAt(int x);
    //getters and setters
    Node getHead();
    Node getTail();
    
private:
    void setHead(Node x);
    void setTail(Node x);
    
};
#endif /* defined(__ArtGallery__LinkedList__) */
