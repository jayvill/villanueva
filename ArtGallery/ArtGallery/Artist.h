//
//  Artist.h
//  ArtGallery
//
//  Created by Jonathan Villanueva on 11/14/15.
//  Copyright (c) 2015 Jonathan Villanueva. All rights reserved.
//

#ifndef __ArtGallery__Artist__
#define __ArtGallery__Artist__

#include <iostream>
#include <string>

#include "Painting.h"

using namespace std;
class Artist{
private:
    string firstName;
    string lastName;
    Painting a_painting;
public:
    Artist();
    Artist(string first_name, string last_name);
};

#endif /* defined(__ArtGallery__Artist__) */
