#include "hw5.h"
#include <stdio.h>
#include<pthread.h>

/*--------------------------------------------------------------------
                Elevator Struct
  --------------------------------------------------------------------- */
struct Elevator{
    pthread_mutex_t lock;//PROTECT ELEVATOR STRUT FROM BEING CHANGED BY MULTIPLE PATHS
    pthread_mutex_t schedule_lock;//ONE PASSANGER HAS CONTROL OF ELEVATOR AT ANY TIME
    pthread_barrier_t floor_arrival;//USED 
    int current_floor;
    int direction;
    int requested_floor;
    enum {ELEVATOR_ARRIVED=1, ELEVATOR_OPEN=2, ELEVATOR_CLOSED=3} state;	
}elevators[ELEVATORS];

/*--------------------------------------------------------------------
                Scheduler_init()
  --------------------------------------------------------------------- */
void scheduler_init() {	/*INITALIZE THE FILEDS IN THE STRUCT*/
    for(int i =0; i < ELEVATORS; i++){
        elevators[i].current_floor=0;		
        elevators[i].direction=-1;
        elevators[i].requested_floor=0;
        elevators[i].state=ELEVATOR_ARRIVED;
        pthread_barrier_init(&elevators[i].floor_arrival, 0, 2);
        pthread_mutex_init(&elevators[i].lock,0);
        pthread_mutex_init(&elevators[i].schedule_lock,0);
    }
}
/*--------------------------------------------------------------------
                Passenger_request
  --------------------------------------------------------------------- */
void passenger_request(int passenger, int from_floor, int to_floor, 
        void (*enter)(int, int), 
        void(*exit)(int, int))
{	
    int elevator = random() % ELEVATORS;
    
    /*LOCK(schedule_lock)*/
    pthread_mutex_lock(&elevators[elevator].schedule_lock);

    elevators[elevator].requested_floor = from_floor; //from_floor is where the passanger request is coming from    
    
    // wait for the elevator to arrive at our origin floor, then get in
    int waiting = 1;
    //while(waiting) { //CHECK IF THE PASSANGER IS CLEARED TO ENTER THE ELEVATOR
      sleep(1);  
        /*BARRIER WAIT*/
        pthread_barrier_wait(&elevators[elevator].floor_arrival);
        /*LOCK(lock)*/
        pthread_mutex_lock(&elevators[elevator].lock);
        //if the elevator is at the called floor and the elevator doors are open
        if(elevators[elevator].current_floor == from_floor && elevators[elevator].state == ELEVATOR_OPEN) {
            enter(passenger, elevator); //the passanger is cleared to enter
            //occupancy++;
            waiting = 0; //break the while loop
            elevators[elevator].requested_floor = to_floor; //requested floor set to destination floor
        }

        /*UNLOCK(lock)*/
        pthread_mutex_unlock(&elevators[elevator].lock);
        /*BARRIER WAIT*/
        pthread_barrier_wait(&elevators[elevator].floor_arrival);
    //}

    // wait for the elevator at our destination floor, then get out
    int riding=1;
    //while(riding) {
        sleep(1);
        /*BARRIER WAIT*/
        pthread_barrier_wait(&elevators[elevator].floor_arrival);
        /*LOCK(lock)*/
        pthread_mutex_lock(&elevators[elevator].lock);
        //if the elevator has arrived at its destination and the doors are open then let the passenger out
        if(elevators[elevator].current_floor == to_floor && elevators[elevator].state == ELEVATOR_OPEN) {
            exit(passenger, elevator);
            //occupancy--;
            elevators[elevator].requested_floor = -1;
            riding = 0; //break the while loop
        }
        /*UNLOCK(lock)*/
        pthread_mutex_unlock(&elevators[elevator].lock);
        /*BARRIER WAIT*/
        pthread_barrier_wait(&elevators[elevator].floor_arrival);
    //}
    /*UNLOCK(schedule_lock)*/
    pthread_mutex_unlock(&elevators[elevator].schedule_lock);
}
/*--------------------------------------------------------------------
                Elevator_ready
  --------------------------------------------------------------------- */
void elevator_ready(int elevator, int at_floor, 
        void(*move_direction)(int, int), 
        void(*door_open)(int), void(*door_close)(int)) {
    //if(elevator!=0) return;

    /*LOCK(lock)*/
    pthread_mutex_lock(&elevators[elevator].lock);
    //pthread_mutex_lock(&lock);
    //pthread_barrier_wait(&elevators[elevator].floor_arrival);
    // if the elevator has arrived at its destination change the state of the elevator and open the doors
    
    /*if(elevators[elevator].state == ELEVATOR_ARRIVED && elevators[elevator].requested_floor == at_floor) {
        door_open(elevator);
        elevators[elevator].state=ELEVATOR_OPEN;
        //UNLOCK(lock)
        pthread_mutex_unlock(&elevators[elevator].lock);
        //BARRIER WAIT
        pthread_barrier_wait(&elevators[elevator].floor_arrival);// wait for passenger to begin entering 
        pthread_barrier_wait(&elevators[elevator].floor_arrival);// wait while the passenger is finishing entering 
        return;
    }*/
    // else if the doors are open then close them
    if(elevators[elevator].state == ELEVATOR_OPEN) { 
        door_close(elevator);
        elevators[elevator].state=ELEVATOR_CLOSED;
    }
    else if(elevators[elevator].state == ELEVATOR_ARRIVED && elevators[elevator].requested_floor == at_floor) {
        door_open(elevator);
        elevators[elevator].state=ELEVATOR_OPEN;
        /*UNLOCK(lock)*/
        pthread_mutex_unlock(&elevators[elevator].lock);
        /*BARRIER WAIT*/
        pthread_barrier_wait(&elevators[elevator].floor_arrival);// wait for passenger to begin entering
        pthread_barrier_wait(&elevators[elevator].floor_arrival);// wait while the passenger is finishing entering
        return;
    } 
    //else if(elevators[elevator].state == ELEVATOR_CLOSED) {
        //return;
    //}    
    else{
        // if there is no one in the elevator
        if(elevators[elevator].requested_floor == -1){
            //pthread_barrier_wait(&elevators[elevator].floor_arrival);
            elevators[elevator].state = ELEVATOR_ARRIVED;
        }
        // if the floor the elevator is at is lower than the requested floor then move elevator upwards
        else if(at_floor < elevators[elevator].requested_floor){
            move_direction(elevator, 1);
            elevators[elevator].current_floor++;
        }
        // if the floor the elevator is at is higher than the requested floor then move elevator downwards
        else if(at_floor > elevators[elevator].requested_floor){ 
            //direction*=-1;
            move_direction(elevator,-1);
            //current_floor=at_floor+direction;
            //state=ELEVATOR_ARRIVED;
            elevators[elevator].current_floor--;
        }
        // the elevator has arrived at this point
        elevators[elevator].state = ELEVATOR_ARRIVED;
    }
    /*UNLOCK(lock)*/
    pthread_mutex_unlock(&elevators[elevator].lock);
}
