This program uses smart pointers and was created and compiled on xcode.
To run on command line:
	make makefile
	./a.out

Known defects:
	when mapping a function such as a function like
		int add_one(int x){
			return x + 1;
		}
	on a LList, the first 2 nodes of the list have incorrect outputs. The first node's value would be added twice and the second node's value would be untouched by the mapped function. All the nodes after the first 2 would then have correct output. I tested the mapped function on the Array also and the output was correct.
	This is an example output:
	first insert
	adding to tail
	adding to head
	adding to head
	adding to head
	adding to head
	1->2->3->9->7->8->
	called map
	3->2->4->10->8->9->//here the first 2 nodes are incorrect
	[4][4][4][4][4]//this is the array input
	called map
	[5][5][5][5][5]//this is the array after the call to map using add_one().
