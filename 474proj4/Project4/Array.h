//
//  Array.h
//  Project4
//
//  Created by Jonathan Villanueva on 11/23/15.
//  Copyright (c) 2015 Jonathan Villanueva. All rights reserved.
//

#ifndef __Project4__Array__
#define __Project4__Array__

#include <iostream>
#include "Collection.h"


class Array : public Collection{
public:
    int *array;
    int size;
    Array(int);
    Array(Array  &Arr);
    virtual Array *copy();//virtual copying scheme
    virtual ~Array();
    virtual int _size();
    virtual bool isEmpty();
    
    //add returns the method reciever
    virtual Array* add(int item, int index); //pure virtual method =0

    //removes all occurences of the integer argument from reciever
    virtual Array* remove(int toRemove);
    
    virtual Array& operator=(Collection *outList){
        return *this;
    }
    
    //returns the element at the index position in the receiver
    //overload [] operator to find index in collection
    //must be able to use this on left hand side
    virtual int& operator[](const int x)
    {
        int size = this->size;
        int element;
        if(x > this->size)
            throw invalid_argument("Index out of bounds");
        else return this->array[x];
        
        /*for(int i = 0; i<size;i++)
        {
            if(this->array[i] == x)
                element = this->array[i];
                return this->array[i];
            //else return *this;
        }
        return element;*/
    }
    //use virtual copy withing assignment operator
    //takes as input a collection instance
    //of the same type as the receiver, that is, the right-hand side in an assignment.
    //deep copies the argument into the receiver. The modified receiver is returned.
    virtual Array& operator=(Collection &var)
    {
        Array* arrayPtr = dynamic_cast<Array*>(&var);
        //arrayPtr->
        //int o_size = dynamic_cast<LList&>(outList)._size();
        //int var_size = dynamic_cast<Array&>(var)._size();
        //check if the sizes are the same
        if(this->size != arrayPtr->_size()){
            cerr<<"Arrays not the same size"<<endl;
            return *this;
        }
        else{
            //int *temp;
            for(int x =0; x<this->size;x++){
                this->array[x] = dynamic_cast<Array&>(var).array[x];
            }
            return *this;
        }
    }
    
    virtual void print(){
        for(int i = 0; i< this->size; i++){
            cout<<"["<<this->array[i]<<"]";
        }
        cout<<endl;
    }
};
#endif /* defined(__Project4__Array__) */
