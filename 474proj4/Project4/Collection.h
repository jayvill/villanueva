//
//  Collection.h
//  Project4
//
//  Created by Jonathan Villanueva on 11/23/15.
//  Copyright (c) 2015 Jonathan Villanueva. All rights reserved.
//

#ifndef __Project4__Collection__
#define __Project4__Collection__

#include <iostream>
using namespace std;
class Collection{ //pure virtual function makes this class abstract class
protected:
        
    //number of elements contained in the receiver
    int size_;
    
public:
    
    //PUBLIC ACCESSORS FOR PROTECTED size_
    int getSize(){
        return size_;
    }
    
    void setSize(int size){
        size_ = size;
        //cout<<"size was: "<< size_ <<" size is now: " << size_;
    }

    
    //add returns the method reciever
    virtual Collection* add(int item, int index) = 0; //pure virtual method =0
    
    //removes all occurences of the integer argument from reciever
    virtual Collection* remove(int toRemove) = 0;
    
    //returns the element at the index position in the receiver
    //overload [] operator to find index in collection
    //must be able to use this on left hand side
    virtual int& operator[](int) = 0;
    
    //takes as input a collection instance
    //of the same type as the receiver, that is, the right-hand side in an assignment.
    //deep copies the argument into the receiver. The modified receiver is returned.
    virtual Collection& operator=(Collection &var) = 0;
    //virtual void* operator=(const Collection &var) = 0;
    
    //virtual copying scheme
    virtual Collection* copy() = 0;
    
    virtual void print() = 0;
    
    virtual bool isEmpty() = 0;
    
    //virtual Collection& operator=(Collection*)=0;
    
    virtual int _size() = 0;
//CONCRETE FUNCTIONS
    
    //default constructor
    Collection();
    
    //copy constructor
    Collection(const Collection&);
    
    //virtual destructor
    virtual ~Collection();
    
    //applies function fn to all elements contained in the receiver
    //Each element in the receiver is replaced by the value returned by fn for
    //that element.
    //The modified reciever is returned
    Collection& map(int (*fn)(int));
    
    //returns a boolean indicating whether the receiver contains the argument integer or not
    bool contains(int);
};

#endif /* defined(__Project4__Collection__) */
