//
//  Node.h
//  Project4
//
//  Created by Jonathan Villanueva on 11/24/15.
//  Copyright (c) 2015 Jonathan Villanueva. All rights reserved.
//

#ifndef __Project4__Node__
#define __Project4__Node__

#include <iostream>
class Node{
    friend class LList;
public:
    Node();
    Node(int data);
    ~Node();
    int data;
    Node* next;
};
#endif /* defined(__Project4__Node__) */
