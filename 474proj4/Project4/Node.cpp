//
//  Node.cpp
//  Project4
//
//  Created by Jonathan Villanueva on 11/24/15.
//  Copyright (c) 2015 Jonathan Villanueva. All rights reserved.
//

#include "Node.h"

Node::Node(void){
    this->next = NULL;
    this->data = NULL;
}

Node::Node(int data){
    this->next = NULL;
    this->data = data;
}

Node::~Node(void){

}

