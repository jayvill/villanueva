//
//  LList.h
//  Project4
//
//  Created by Jonathan Villanueva on 11/23/15.
//  Copyright (c) 2015 Jonathan Villanueva. All rights reserved.
//

#ifndef __Project4__LList__
#define __Project4__LList__

#include <iostream>
#include "Collection.h"

//LList inherits from abstract class Collection
//LList implements all the deferred Collection methods, appropriate default and
//copy constructors, and a virtual destructor and virtual copying schemes.
//allow clients of LList to invoke the inherited methods map() and contains()

#include "Node.h"

class Node;
using namespace std;

class LList : public Collection{
public:
    int size;
    Node* head;
    Node* tail;
    LList();
    LList(int size);
    LList(const LList&);
    virtual bool isEmpty();
    virtual void print();
    virtual int _size();
    //implement abstract methods from base class
    virtual LList* add(int item, int index);
    virtual LList* remove(int toRemove);
    
    //return the element at the specified index position
    //virtual void* operator[](int x);
    virtual int& operator[](int x)
    {
        
        int cntr = 0;
        Node* pointer = this->head;
        //cout<<pointer<<endl;
        //error if param > size
        if(x > this->size)
        {
            cerr<<"in operator[], param > size"<<endl;
            throw invalid_argument("ERROR");
        }
        else
        {
            if(x == 1){
                return pointer->data;
            }
            if(pointer == NULL){
                throw invalid_argument("ERROR pointer NULL");
            }
            while(pointer != NULL)
            {
                if(cntr == x){
                    return pointer->data;
                }
                else{
                    pointer = pointer->next;
                    cntr++;
                }
            }
            return pointer->data;
        }
    }
    virtual LList& operator=(Collection &outList){
        int o_size = dynamic_cast<LList&>(outList)._size();
        //check if the lists are the same size
        if(this->size == o_size){
            //copy size from outList
            this->size = dynamic_cast<LList&>(outList)._size();
            Node* ntemp = dynamic_cast<LList&>(outList).head;
            Node* ntemp2 = this->head;
            //copy outList values into receiver
            while(ntemp!=NULL){
                ntemp2->data = ntemp->data;
                ntemp2 = ntemp2->next;
                ntemp = ntemp->next;
            }
            //delete old list
            LList temp = dynamic_cast<LList&>(outList);
            //temp.~LList();
            //dynamic_cast<LList&>(outList) = NULL;
            return *this;
        }
        else{
            cerr<<"Lists not the same size"<<endl;
            return *this;
        }
        //delete old list
        //LList temp = dynamic_cast<LList&>(outList);
        //temp.~LList();
        //dynamic_cast<LList&>(outList) = NULL;
        
        //delete old list
        /*
        Node* temp = dynamic_cast<LList&>(outList).head->next;
        while(dynamic_cast<LList&>(outList).head!=NULL){
            delete dynamic_cast<LList&>(outList).head;
            dynamic_cast<LList&>(outList).head = temp;
            temp = dynamic_cast<LList&>(outList).head->next;
        }
        dynamic_cast<LList&>(outList).head = NULL;
        dynamic_cast<LList&>(outList).tail = NULL;
        */
    }
    
    //deep copy the argument into the receiver. Return the modified reciever
    //param a collection instance of the same type as the receiver
    //virtual void* operator=(const LList &outList);
    //virtual LList& operator=(Collection outList)
    //{
        
      //  LList* ptr = dynamic_cast<LList*>(outList)->copy();
       // return *ptr;
        //LList *aList = new LList(*(dynamic_cast<LList*>(outList)));
        //dynamic_cast<LList*>(outList)->~LList();
        //return *aList;
    //}

    virtual ~LList();
    
    virtual LList* copy();
    //{
      //  LList *aList = new LList(*this);
        //return aList;
    //}
};
#endif /* defined(__Project4__LList__) */
