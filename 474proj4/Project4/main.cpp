//
//  main.cpp
//  Project4
//
//  Created by Jonathan Villanueva on 11/23/15.
//  Copyright (c) 2015 Jonathan Villanueva. All rights reserved.
//

#include <iostream>
#include "Collection.h"
#include "LList.h"
#include "Array.h"

int add_one(int x){
    return x+1;
}

int main(int argc, const char * argv[])
{
    //int (*integer) () = 0;
    // insert code here...
    //std::cout << "Hello, World!\n";
    
    
    //Collection *c = new LList(6);
    //cout<<"LList size = "<<c->getSize()<<endl;
    ////c->add(4, 5);
    //int x = c->operator[](2);
    //cout<<x<<endl;
    //c->remove(4);
    //c->print();
    //c->isEmpty();
    
    
    //Collection *l = new LList();
    //LList aList;
    //l->add(7, 0);
    //l->add(8, 1);
    //l->add(8, 2);
    //l->add(8, 2);
    //l->add(8, 1);
    //l->add(8, 5);
    
    
    //l->remove(8);
    //l->print();
    
    //Collection *a = new Array(6);
    //cout<<"Array size = "<<a->getSize()<<endl;
    //a->add(5, 5);
    //cout<< a->operator[](2)<<endl;
    //a->operator[](2) = 7;
    //a->print();
    //cout<<a->operator[](2)<<endl;
    
    //Collection *b = new Array(6);
    //b->operator[](2) = 8;
    //int x = b->operator[](2);
    //cout<<"x = "<<x<<endl;
    
    //cout<<"index: "<<l->operator[](0)<<endl;
    //cout<<c->isEmpty()<<endl;
    //c->operator=(*l);
    
    //c->operator=(*l);
    
    //cout<<"l-list: ";
    //c->print();
    
    //l->print();
    //l->remove(8);
    //l->print();
    
    //a->operator=(*b);
    
    //a->print();
    //b->print();
    
    //cout<<l->contains(8)<<endl;
    //cout<<a->contains(8)<<endl;
    //Collection *c ;
    
    LList aList;
    Array array(5);
    Collection *pCollection = &aList;
    Collection *arrayCollection = &array;
    
    pCollection->add(7, 0);
    pCollection->add(8, 1);
    pCollection->add(9, 0);
    pCollection->add(3, 0);
    pCollection->add(2, 0);
    pCollection->add(1, 0);
    
    pCollection->print();
    pCollection->map(&add_one);
    pCollection->print();
    
    arrayCollection->operator[](0) = 4;
    arrayCollection->operator[](1) = 4;
    arrayCollection->operator[](2) = 4;
    arrayCollection->operator[](3) = 4;
    arrayCollection->operator[](4) = 4;
    
    arrayCollection->print();
    arrayCollection->map(&add_one);
    arrayCollection->print();
    /*
    Collection *c = new LList();
    c->add(4,0);
    c->add(3,0);
    c->add(1,2);
    c->add(11,1);
    c->add(14,0);
    //c->add(3,1);
    //c->add(11,3);
    //cout<<c->getSize()<<endl;
    //cout<<c->contains(4)<<endl;
    c->print();
    
    /*
    Collection *list = new LList();
    Collection *newlist;
    list->add(1, 0);
    list->add(2, 1);
    list->add(3, 2);
    cout<<"list contains: ";
    list->print();
    list->map(&add_one);
    cout<<"new list : ";
    list->print();
    return 0;
    */
}

