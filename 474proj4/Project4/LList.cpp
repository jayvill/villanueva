//
//  LList.cpp
//  Project4
//
//  Created by Jonathan Villanueva on 11/23/15.
//  Copyright (c) 2015 Jonathan Villanueva. All rights reserved.
//

#include "LList.h"
#include "Node.h"

//default constructor
LList::LList(){
    this->head = NULL;
    this->tail = NULL;
    this->size = 0;
    this->setSize(0);
}

LList::LList(int size){
    if(size == 0) cerr<<"specify size > 0"<<endl;
    this->size = size;
    int counter = size;
    this->head = new Node();
    this->tail = this->head;
    Node*temp1 = this->head;
    Node*temp2 = this->tail;
    counter = 1;
    while(counter != size){
        temp2 = new Node();
        tail = temp2;
        temp1->next = temp2;
        temp1 = temp1->next;
        counter++;
    }
    cout<<"created a list of size "<<counter<<endl;
    this->setSize(size);
    //this->tail = this->head;
    //this->size = size;
    //Node* temp = this->head;
    //Node* node = new Node();
    //while()
}

//copy constructor
LList::LList(const LList &list)
{
    if(list.head == NULL){
        this->head = NULL;
        this->tail = NULL;
    }
    else
    {
        this->head = new Node();
        this->size = 1;
        this->tail = this->head;
        Node *temp = this->head;
        Node *list_ptr = list.head;
        while(list_ptr != NULL)
        {
            temp->data = list_ptr->data;
            if(list_ptr->next != NULL)
            {
                temp->next = new Node();
                this->size+=1;
                tail = temp->next;
                temp = tail;
            }
            list_ptr = list_ptr->next;
        }
    }
}

LList *LList::copy(){
    LList *aList = new LList(*this);
    return aList;
}

LList::~LList(){
    Node* temp = head;
    while(head!=NULL){
        //delete head;
        //head = temp;
        temp = head->next;
        delete temp;
        head = temp;
        if(temp == NULL) return;
    }
    this->head = NULL;
    this->tail = NULL;
    //delete(this);
}

/*LList *LList::copy(){
    LList *l = new LList(*this);
    return l;
}*/

LList* LList::add(int item, int index)
{
    Node* temp = new Node(item); //this is the node to add
    //Node* pointer;// = this->head;
    //Node* prev;// = this->head;
    //int counter = 1;
    //add to the non empty list
    
    if(index < 0){
        delete temp;
        throw invalid_argument("Linked list begins at index 0");
    }
    if(index > this->size){
        delete temp;
        throw invalid_argument("Index too large");
    }
    else if(this->size == 0)//first insert of list
    {
        cout<<"first insert"<<endl;
        //temp->data = item;
        temp->next= NULL;
        this->head = temp;
        this->tail = temp;
        this->size = this->size+=1;
        this->setSize(this->size);
    }
    //case adding at the head
    else if(this->size != 0 && index == 0){//if list not empty and want to add at beginning
        cout<<"adding to head"<<endl;
        temp->next = this->head;
        //temp->data = item;
        this->head = temp;
        this->size = this->size+=1;
        this->setSize(this->size);
    }
    //case adding at the tail
    else if(this->size != 0 && index == size){
        cout<<"adding to tail"<<endl;
        this->tail->next = temp;
        temp->next = NULL;
        this->tail = temp;
        this->size = this->size+=1;
        this->setSize(this->size);
    }
    else
    {
        int cntr = 0;
        Node* pointer = this->head;
        Node* prev = this->head;
        while(pointer != NULL)
        {
            if(cntr == index)
            {
                //add node
                cout << "adding node with data: " << temp->data << " to position: " << cntr << endl;
                prev->next = temp;
                temp->next = pointer;
                this->size+=1;
                this->setSize(this->size);
                //cntr+=1;
                return this;
            }
            else{
                cntr+=1;
                pointer = pointer->next;
                if(cntr > 1)
                    prev = prev->next;
            }
        }
    }
    /*
    //check if adding to an empty list
    if (this->head == NULL) {
        this->head = new Node();
        this->tail = this->head;
        pointer = this->head;
        prev = this->head;
        Node *temp_ = this->head;
    }
    else{pointer = this->head; prev = this->head;}
    
    //error check
    if(index > this->size+1)
    {
        cerr<<"specified index is too large"<<endl;
        return NULL;
    }
    //case adding at the head
    if(index == 1)
    {
        cout<<"adding node to the head of the list"<<endl;
        this->head = temp;
        temp->next = pointer;
        this->size+=1;
    }
    //case adding at the tail
    else if(index == this->size+1)
    {
        cout<<"adding node to end of the list"<<endl;
        this->tail->next = temp;
        this->tail = temp;
        this->size+=1;
    }
    else
    {
        while(counter <= index)
        {
            if(counter == index)
            {
                //add node
                cout << "adding node with data: " << temp->data << " to position: " << counter << endl;
                prev->next = temp;
                temp->next = pointer;
                this->size+=1;
                counter+=1;
            }
            else //increment pointers and counter
            {
                counter+=1;
                pointer = pointer->next;
                if(counter > 2)
                    prev = prev->next;
            }
        }
    }//end else
    this->setSize(this->size);
    cout<<"This list size = "<<this->size<<endl;
    return this;
     */
    //cout<<"Tail = "<<this->tail->data<<endl;
    return this;
}//end LList::add()

/*
    remove all occurrences of the integer argument from the receiver
    the modified receiver is returned
 */
LList* LList::remove(int toRemove){
    Node* pointer = this->head;
    Node* prev = this->head;
    int counter = 0;
    while(pointer != NULL){
        cout<<"head is = "<<this->head->data<<endl;
        if(pointer->data == toRemove)
        {
            //if toRemove is at the head
            if(pointer == this->head)
            {
                this->head = this->head->next;
                pointer->next = NULL;
                pointer = this->head;//pointer now points to the new head
                delete(prev);
                cout<<"deleted head "<< prev->data<<endl;
                this->setSize(this->size--);
                prev = this->head;//prev now points to the new head
                continue;
            }
            
            //if toRemove is at the tail
            else if(pointer == this->tail)
            {
                this->tail = prev;
                prev->next = NULL;
                delete(pointer);
                pointer = this->tail;//set pointer to the new tail
                this->setSize(this->size--);
                cout<<"deleted tail "<< pointer->data<<endl;
            }
            
            //if toRemove is in the middle
            else
            {
                Node* pointerNext = pointer->next;
                pointer->next = NULL;
                prev->next = pointerNext;
                delete(pointer);
                cout<<"deleted middle "<< pointer->data<<endl;
                this->setSize(this->size--);
                pointer = pointerNext;
                continue;//counters have already been updated so skip iteration
            }
        }
        
        //update counters
        pointer=pointer->next;
        counter+=1;
        if(counter > 1)
            prev = prev->next;//note prev points to null after removing tail
    }
    
    return this;
}
/*
 void* LList::operator[](int x)
{
    int cntr = 1;
    Node* pointer = this->head;
    //error if param > size
    if(x > this->size)
    {
        cerr<<"in operator[], param > size"<<endl;
        return NULL;
    }
    else
    {
        if(x == 1){
            return pointer;
        }
        while(cntr != x)
        {
            if(pointer == NULL){
                return NULL;
            }
            else{
                pointer = pointer->next;
                cntr++;
            }
        }
        return pointer;
    }
}

void* LList::operator=(const LList &outList)
{
    outList = new LList(this->size);
    
    return NULL;
}
*/
bool LList::isEmpty(){
    if(this->head == NULL){
        return true;
    }
    else return false;
}

void LList::print(){
    Node *temp = this->head;
    while(temp!=NULL){
        cout<<temp->data;
        cout<<"->";
        temp=temp->next;
    }
    cout<<endl;
}

int LList::_size(){
    return this->size;
}