//
//  Collection.cpp
//  Project4
//
//  Created by Jonathan Villanueva on 11/23/15.
//  Copyright (c) 2015 Jonathan Villanueva. All rights reserved.
//

#include "Collection.h"

Collection::Collection(){
    size_ = 0;
}

//copy constructor
Collection::Collection(const Collection& collection){
    this->size_ = collection.size_;
}

Collection::~Collection(){
    //delete this;
}

Collection&::Collection::map(int (*fn)(int)){
    cout<<"called map"<<endl;
    int x = 0;
    for(int i=0; i<this->size_;i++)
    {
        //cout<<this->operator[](i)<<endl;
        (*this)[i] = fn((*this)[i]);
        //apply the function to the index
        //x = (*fn)(this->operator[](i));
        //cout<<"map value = "<<x<<endl;
        //this->operator[](i) = x;
    }
    return *this;
}

bool Collection::contains(int x){
    //use indexing operator
    for(int i=0;i<this->size_; i++)
    {
        if(this->operator[](i) == x)
            return true;
        //if((*this)[i] == x){
          //  return true;
        //}
    }
    return false;
}