//
//  Array.cpp
//  Project4
//
//  Created by Jonathan Villanueva on 11/23/15.
//  Copyright (c) 2015 Jonathan Villanueva. All rights reserved.
//

#include "Array.h"

Array::Array(int size){
    this->array = new int[size]; //create an array of specified size
    this->setSize(size);
    this->size = size;
}

//copy constructor
Array::Array(Array &array){
    int size = array.Collection::getSize();
    if(array.array == NULL){
        this->array = NULL;
    }
    else{
        this->array = new int[size];
        this->setSize(size);
        //loop through array to deep copy values
        for(int i=0; i<size;i++){
            this->array[i] = array.array[i];
        }
    }
}

Array *Array::copy(){
    Array *a = new Array(*this);
    return a;
}

Array::~Array(){
    delete [] array;
    array = NULL;
}

Array* Array::add(int item, int index)
{
    cerr<<"Array size is already set. Cannot grow the array. Add failed"<<endl;
    return NULL;
}

Array* Array::remove(int toRemove)
{
    cerr<<"Array size is already set. Cannot shrink the array. Remove failed"<<endl;
    return NULL;
}

bool Array::isEmpty()
{
    if(this->size == 0 || this->array == NULL)
    {
        return true;
    }
    else return false;
    /*for(int i = 0; i<this->size;i++)
    {
        if(this->array[i] == NULL)
            return true;
    }
    return false;*/
}

int Array::_size(){
    return this->size;
}


